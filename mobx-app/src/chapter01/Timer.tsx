import * as React from 'react'
import { makeAutoObservable } from 'mobx'
import { observer } from 'mobx-react'

class MobXTimer {
  secondsPassed = 0

  constructor () {
    makeAutoObservable(this)
  }

  increase (): void {
    this.secondsPassed += 1
  }

  reset (): void {
    this.secondsPassed = 0
  }
}

const Timer_: React.FC = () => {
  const [timer] = React.useState<MobXTimer>(() => new MobXTimer())

  React.useEffect(() => {
    const intervalId = setInterval(() => {
      timer.increase()
    }, 1000)
    return () => {
      clearInterval(intervalId)
    }
    // eslint-disable-next-line
  }, [])

  return (
    <button onClick={() => timer.reset()}>Seconds passed: {timer.secondsPassed}</button>
  )
}

export const Timer = observer(Timer_)
