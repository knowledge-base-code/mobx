import * as React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { Timer } from './Timer'

describe('<Timer />', () => {
  beforeEach(() => {
    jest.useFakeTimers()
  })

  afterEach(() => {
    jest.runOnlyPendingTimers()
    jest.useRealTimers()
  })

  test('renders a button with Seconds passed', () => {
    render(<Timer />)
    const btnElement = screen.getByText(/Seconds passed: \d/i)
    expect(btnElement).toBeInTheDocument()
  })

  test('clicks reset the timer', () => {
    render(<Timer />)

    userEvent.click(screen.getByText(/Seconds passed: \d/i))
    expect(screen.getByText('Seconds passed: 0')).toBeInTheDocument()
  })
})
